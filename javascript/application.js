window.application = angular.module("ticket-templating-tool", [])

window.application.controller("fabric", function ($scope) {
    var canvas = null;
    var shapes = [];
    var images = [];
    
    $scope.selection = null;
    $scope.selectionLeft = null;
    $scope.selectionTop = null;
    $scope.isWithoutPictures = true;
    
    $scope.save = function () {
        var canvasElement;
        var saved;
        
        canvasElement = document.getElementById("test");
        canvasElement.toDataURL("/var/www/images/" + new Date().toISOString() + ".png");
    };
    
    var handleSelection = function (options) {
        console.log("something was selected", options);
        
        if (options.target) {
            $scope.selection = options.target;
            $scope.selectionLeft = options.target.get("left");
            $scope.selectionTop = options.target.get("top");
            $scope.$apply();
        } else {
            $scope.selection = null;
            $scope.$apply();
        }
    };
    
    var handleMovement = function (options) {
        console.log("something was moved");
        
        if ($scope.selection) {
            $scope.selectionLeft = Math.round($scope.selection.get("left"));
            $scope.selectionTop = Math.round($scope.selection.get("top"));
            $scope.$apply();
        }
    }
    
    $scope.isCanvasPresent = false;
    
    $scope.width = 960
    $scope.height = $scope.width / 2;
    
    $scope.box = {
        width: 60,
        height: 60
    };
    
    $scope.image = {
        isGrey: false,
        width: 120,
        height: 120,
    };
    
    $scope.render = function () {
        $scope.isCanvasPresent = true;
        
        canvasWrapper = angular.element(".canvas-wrapper");
        canvasWrapper.show();
        canvasWrapper.height($scope.height);
        canvasWrapper.width($scope.width);
        
        canvas = new fabric.Canvas("test");
        canvas.setDimensions($scope.width, $scope.height);
        
        canvas.on("mouse:down", handleSelection);
        canvas.on("mouse:move", handleMovement);
        canvas.on("after:render", function() {
            canvas.calcOffset(); // this is awesome
        });
        
        canvas.renderAll();
    };
    
    $scope.addBox = function () {
        var box = new fabric.Rect({
            width: $scope.box.width,
            height: $scope.box.height,
            left: (canvas.width / 2) - ($scope.box.width / 2),
            top: (canvas.height / 2) - ($scope.box.height / 2)
        });
        
        shapes.push(box);
        canvas.add(box);
    };
    
    $scope.addCat = function () {
        $scope.isWithoutPictures = false;
        
        isGreyAddition = $scope.image.isGrey ? "g/" : "";
        
        fabric.Image.fromURL("http://placekitten.com/" + isGreyAddition + $scope.image.width + "/" + $scope.image.height, function(oImg) {
            images.push(oImg);
            canvas.add(oImg);
        });
    };
    
    $scope.$watch("selectionLeft", function (newValue, oldValue) {
        console.log("values change");
        
        if ($scope.selection) {
            $scope.selection.set("left", newValue);
            canvas.renderAll();
        }
    });
    
    $scope.$watch("selectionTop", function (newValue, oldValue) {
        console.log("values change");
        
        if ($scope.selection) {
            $scope.selection.set("top", newValue);
            canvas.renderAll();
        }
    });
});